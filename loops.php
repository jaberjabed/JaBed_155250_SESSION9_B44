<?php


//Foreach Function Start Here 


$ageArray = array("Abul"=>32,"Kuddus"=>30,"Kajol"=>35,"Deepika"=>31);

foreach($ageArray as $key=>$value){

    echo"ageArray['$key'] is $value<br>";
}
echo "<br><br>";

$ageArray = array("Abul"=>32,"Kuddus"=>30,"Kajol"=>35,"Deepika"=>31);

foreach($ageArray as $value) {

    echo "ageArray is $value<br>";
}




// Print 1 to 16 With Table
$myArray = array(array());
$count = 1;

echo "<table border='4'>";
for($i = 0; $i<=3; $i++){
    echo "<tr>";
    for($j=0; $j<=3; $j++){

        $myArray[$i][$j] = $count;
        echo "<td>";
        print_r($myArray[$i][$j]);
        echo "</td>";
        $count++;
    }
    echo "</tr>";
}

echo "</table>";



// For Loop Practice with better Known
$myArray = array(array());
$count = 1;

for($i = 0; $i<=3; $i++){

    for($j=0; $j<=3; $j++){

        $myArray[$i][$j] = $count;
        $count++;
    }
}


echo "<pre>";
var_dump($myArray);
echo "</pre>";



//For Loop

echo "<br>";

for ($i=1 ; $i<=10; $i++ ){

    echo "Hi $i ";

}

//Do While Loop Start to know here

$i = 12;
do {
    echo "Hello $i <br>";
    $i++;
}
while($i <=10);


// While Loop Practice
$i = 0;

while(1){

    if($i > 10){
        break;
    }

    echo $i . " ";

    $i = $i +2;

} // Here end of while loop


echo "PHP - ";
$i = 1;
while ($i <= 44){

    if($i == 44){
        echo "B$i.";
        break;
    }
        echo "B$i, ";
        $i++;
    }

*/